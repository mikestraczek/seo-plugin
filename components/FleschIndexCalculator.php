<?php namespace NextLevels\Seo\Components;

use Cms\Classes\ComponentBase;
use NextLevels\Seo\Helpers\SeoHelper;

/**
 * Class FleschIndexCalculator
 */
class FleschIndexCalculator extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'Flesch Index Rechner',
            'description' => 'Berechnet die Lesbarkeit eines Textes.'
        ];
    }

    /**
     * Calculate given text
     *
     * @return array
     * @throws \ValidationException
     */
    public function onCalculateIndex()
    {
        $text = \Input::get('calculate-text');
        $rules = ['calculate-text' => 'required|max:1000'];
        $customMessages = [
            'calculate-text.required' => 'Es muss ein Text eingetragen werden.',
            'calculate-text.max'      => 'Text darf maximal 1000 Zeichen lang sein.'
        ];
        $validator = \Validator::make(['calculate-text' => $text], $rules, $customMessages);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        return ['#reading-ease-score' => (new SeoHelper)->calculateReadingEase($text)];
    }
}
