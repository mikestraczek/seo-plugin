<?php namespace NextLevels\Seo;

use Cms\Classes\Page;
use Cms\Controllers\Index;
use NextLevels\Seo\Components\FleschIndexCalculator;
use NextLevels\Seo\Helpers\SeoHelper;
use NextLevels\Seo\Observers\PostObserver;
use RainLab\Blog\Models\Post;
use System\Classes\PluginBase;

/**
 * Class Plugin
 */
class Plugin extends PluginBase
{

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            FleschIndexCalculator::class => 'fleschIndexCalculator'
        ];
    }

    /**
     * On boot
     *
     * @return array|void
     */
    public function boot()
    {
        if (class_exists(RainLab\Blog\Models\Post::class)) {
            Post::observe(PostObserver::class);
            Post::extend(function (Post $post) {
                $post->addDynamicMethod('getReadingEaseScoreAttribute', function () use ($post) {
                    return (new SeoHelper())->calculateReadingEase($post->content);
                });

                $post->addDynamicMethod('getReadingEaseTextAttribute', function () use ($post) {
                    return (new SeoHelper())->getReadingEaseScoreText($post->readingEaseScore);
                });
            });
        }

        \Event::listen('backend.form.extendFields', function ($widget) {

            if (! $widget->getController() instanceof Index) {
                return;
            }

            if (! $widget->model instanceof Page) {
                return;
            }

            $widget->addTabFields([
                'settings[meta_keywords]'       => [
                    'tab'              => 'cms::lang.editor.meta',
                    'label'            => 'Meta Keywords',
                    'comment'          => 'Durch Kommas trennen.',
                    'type'             => 'taglist',
                    'separator: space' => 'comma'
                ],
                'open_graph_section'            => [
                    'tab'   => 'cms::lang.editor.meta',
                    'label' => 'Open Graph',
                    'type'  => 'section'
                ],
                'settings[meta_og_title]'       => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'Titel',
                    'comment' => 'Hier tragen Sie den Titel ein, der beim Teilen einer Seite angezeigt werden soll.'
                ],
                'settings[meta_og_description]' => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'Beschreibung',
                    'comment' => 'Hier tragen Sie eine kurze, ansprechende Beschreibung des Seiteninhalts ein.',
                    'type'    => 'textarea',
                    'size'    => 'size'
                ],
                'settings[meta_og_type]'        => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'Typ',
                    'comment' => 'Mit diesem Tag geben Sie an, um welche Art von Inhalt es sich handelt. In der Regel wird hier „Website“ hinterlegt, weitere Möglichkeiten finden Sie auf der offizielle Open Graph Protocol-Seite unter http://ogp.me.'
                ],
                'settings[meta_og_url]'         => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'URL',
                    'comment' => 'Hier hinterlegen Sie die URL, die geteilt werden soll. Dadurch stellen Sie sicher, dass konsequent dieselbe URL geteilt wird und verhindern eine Verfälschung durch Session-IDs, Suchparameter oder ähnliches.'
                ],
                'settings[meta_og_site_name]'   => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'Name der Seite',
                    'comment' => 'Hier tragen Sie den offiziellen Namen Ihrer Seite ein.'
                ],
                'settings[meta_og_image]'       => [
                    'tab'     => 'cms::lang.editor.meta',
                    'label'   => 'BILD-URL',
                    'comment' => 'Hier tragen Sie die URL eines Bildes an, das beim Teilen oder Liken Ihrer Seite angezeigt werden soll.',
                    'type'    => 'mediafinder',
                    'mode'    => 'image'
                ]
            ]);
        });
    }
}
