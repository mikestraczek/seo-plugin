<?php namespace NextLevels\Seo\Helpers;

/**
 * Class SeoHelper
 */
class SeoHelper
{

    /**
     * @var string
     */
    protected $contentText = '';

    /**
     * Count content words
     *
     * @return int
     */
    protected function countContentWords()
    {
        return count(array_filter(explode(' ', preg_replace('/[\n\r]/', ' ', $this->contentText))));
    }

    /**
     * Count content sentences
     *
     * @return int
     */
    protected function countContentSentences()
    {
        return count(array_filter(preg_split('/[.!?]/', preg_replace('/[\n\r]/', ' ', $this->contentText))));
    }

    /**
     * Count content syllables
     *
     * @return int
     */
    protected function countContentSyllables()
    {
        $syllable = new \Syllable('de');
        $syllable->setCache(new \Syllable_Cache_Json(storage_path('framework/cache')));
        $syllable->setHyphen(' ');

        return $syllable->countSyllablesText($this->contentText) + 1;
    }

    /**
     * Calculate reading ease score
     *
     * @param string $text
     *
     * @return float|int
     */
    public function calculateReadingEase($text = '')
    {
        $this->contentText = $text;

        if (! empty($this->contentText)) {
            $numberOfWords = $this->countContentWords();
            $numberOfSentence = $this->countContentSentences();
            $numberOfSyllables = $this->countContentSyllables();

            $ASL = $numberOfWords / $numberOfSentence;
            $ASW = $numberOfSyllables / $numberOfWords;

            return round(180 - $ASL - (58.5 * $ASW)) <= 100 ? round(180 - $ASL - (58.5 * $ASW)) : 100;
        }

        return 0;
    }

    /**
     * Get reading ease score text with given score
     * @param int $score
     *
     * @return string
     */
    public function getReadingEaseScoreText($score = 0)
    {
        $difficult = 'Sehr schwer';

        switch (true) {
            case $score <= 30:
                $difficult = 'Sehr schwer';
                break;
            case $score <= 50:
                $difficult = 'Schwer';
                break;
            case $score <= 60:
                $difficult = 'Mittelschwer';
                break;
            case $score <= 70:
                $difficult = 'Mittel';
                break;
            case $score <= 80:
                $difficult = 'Mittelleicht';
                break;
            case $score <= 90:
                $difficult = 'Leicht';
                break;
            case $score <= 100:
                $difficult = 'Sehr leicht';
                break;
        }

        return $difficult;
    }
}
